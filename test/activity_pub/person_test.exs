defmodule ActivityPub.PersonTest do
  use ActivityPub.DataCase, async: true

  alias ActivityPub.Person

  test "implement	inheritance" do
    require ActivityPub

    {:ok, person} = Person.new()
    assert ActivityPub.is_ap(person)
    assert ActivityPub.is_object(person)
    assert ActivityPub.is_actor(person)
    refute ActivityPub.is_activity(person)
    refute ActivityPub.is_link(person)
  end
end
