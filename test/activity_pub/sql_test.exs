defmodule ActivityPub.SQLTest do
  use ActivityPub.DataCase, async: true

  test "insert and load" do
    assert {:ok, per} = ActivityPub.Person.new(%{content: "content", id: "alex", preferred_username: "alex_castano"})
    assert {:ok, per_sql} = ActivityPub.SQL.insert(per)
    IO.inspect(per_sql)

    assert loaded_per = ActivityPub.SQL.load_actor("alex")
    IO.inspect(loaded_per)
    assert per == loaded_per

    assert {:ok, obj} = ActivityPub.Object.new(%{extra: "field", content: "content", id: "obj_id"})
                        |> IO.inspect()
    assert {:ok, obj_sql} = ActivityPub.SQL.insert(obj)
    IO.inspect(obj_sql)

    assert loaded_obj = ActivityPub.SQL.load_object("obj_id")
    IO.inspect(loaded_obj)
    assert obj == loaded_obj
  end
end
