defmodule ActivityPubTest do
  use ActivityPub.DataCase, async: true

  alias ActivityPub.{Object, Person, Create}

  describe "parse" do
    test "works" do
      map = %{
        attributed_to: [
          %{
            name: "Alex"
          },
          "https://doug.gitlab.com",
        ],
        type: "Object",
        content: "This is a content",
        name: "This is my name",
        end_time: "2015-01-01T06:00:00-08:00"
      }

      assert {:ok, %Object{} = o} = ActivityPub.parse(map)
      assert o.content == map.content
      assert o.name == map.name
    end

    test "has extensions" do
      map = %{
        attributed_to: [
          %{
            name: "Alex"
          },
          "https://doug.gitlab.com",
        ],
        extra: "field",
        type: "Object",
        content: "This is a content",
        name: "This is my name",
        end_time: "2015-01-01T06:00:00-08:00"
      }

      assert {:ok, %Object{} = o} = ActivityPub.parse(map)
      assert o.content == map.content
      assert o.name == map.name
      assert o[:name] == map.name
      assert o["name"] == map.name
      assert o[:extra] == map.extra
      assert o["extra"] == map.extra
    end

    test "cannot receive a nil in a list" do
      map = %{
        attributed_to: [
          %{
            name: "Alex"
          },
          "https://doug.gitlab.com",
          nil
        ],
        type: "Object",
        content: "This is a content",
        name: "This is my name",
        end_time: "2015-01-01T06:00:00-08:00"
      }

      assert {:error, ch} = ActivityPub.parse(map)
      assert "is invalid" in errors_on(ch).attributed_to
    end

    test "cannot receive a list in a list" do
      map = %{
        attributed_to: [
          %{
            name: "Alex"
          },
          "https://doug.gitlab.com",
          []
        ],
        type: "Object",
        content: "This is a content",
        name: "This is my name",
        end_time: "2015-01-01T06:00:00-08:00"
      }

      assert {:error, ch} = ActivityPub.parse(map)
      assert "is invalid" in errors_on(ch).attributed_to
    end

    test "validates datetime fields" do
      map = %{
        attributed_to: [
          %{
            name: "Alex"
          },
          "https://doug.gitlab.com",
        ],
        type: "Object",
        content: "This is a content",
        name: "This is my name",
        end_time: "2015-01-01T26:00:00-08:00"
      }

      assert {:error, ch} = ActivityPub.parse(map)
      assert "is invalid" in errors_on(ch).end_time
    end

    test "parses Person type" do
      map = %{
        type: "Person",
        content: "This is a content",
        name: "This is my name",
        preferred_username: "Alex"
      }

      assert {:ok, %Person{} = p} = ActivityPub.parse(map)
      assert p.content == map.content
      assert p.name == map.name
      assert p.preferred_username == map.preferred_username
    end

    test "parses Create Activity" do
      map = %{
        "@context": "https://www.w3.org/ns/activitystreams",
        "summary": "Sally created a note",
        "type": "Create",
        "actor": %{
          "type": "Person",
          "name": "Sally"
        },
        "object": %{
          "type": "Note",
          "name": "A Simple Note",
          "content": "This is a simple note"
        }
      }

      assert {:ok, %Create{} = p} = ActivityPub.parse(map)
    end
  end
end
