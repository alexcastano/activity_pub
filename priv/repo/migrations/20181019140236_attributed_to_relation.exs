defmodule ActivityPub.Repo.Migrations.AttributedToRelation do
  use Ecto.Migration

  def change do
    create table("attributed_to_relation", primary_key: false) do
      add(
        :actor_id,
        references("activity_pub_objects",
          column: :__local_id,
          on_delete: :delete_all,
          on_update: :update_all
        )
      )
      add(
        :object_id,
        references("activity_pub_objects",
          column: :__local_id,
          on_delete: :delete_all,
          on_update: :update_all
        )
      )
    end

    create(unique_index("attributed_to_relation", [:actor_id, :object_id]))
  end
end
