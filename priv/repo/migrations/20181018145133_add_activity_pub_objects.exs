defmodule ActivityPub.Repo.Migrations.AddActivityPubObjects do
  use Ecto.Migration

  def change do
    create table("activity_pub_objects", primary_key: false) do
      add :__local_id, :bigserial, primary_key: true
      add :id, :string, size: 1024
      add :type, :string, size: 1024
      add :content, :string, size: 1024
      add :name, :string, size: 1024
      add :end_time, :utc_datetime
      add :__extensions, :map, default: %{}
    end

    create unique_index("activity_pub_objects", :id)
  end
end
