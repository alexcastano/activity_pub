defmodule ActivityPub.Repo.Migrations.AddActivityPubActors do
  use Ecto.Migration

  def change do
    create table("activity_pub_actors", primary_key: false) do
      add(
        :__local_id,
        references("activity_pub_objects",
          column: :__local_id,
          on_delete: :delete_all,
          on_update: :update_all
        ),
        primary_key: true
      )

      add(:preferred_username, :string, size: 1024)
    end
  end
end
