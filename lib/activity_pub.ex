defmodule ActivityPub do
  @moduledoc """
  ActivityPub keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def load(_id), do: {:ok, nil}

  def parse(%{} = input) do
    input = normalize_input(input)
    type = Map.get(input, "type")
    module = to_activity_pub(type)
    module.new(input)
  end

  defp normalize_input(%{} = input) do
    input
    |> Enum.into(%{}, fn {key, value} ->
      key = key |> to_string() |> Recase.to_snake()
      {key, value}
    end)
  end

  def to_activity_pub([head | _]), do: to_activity_pub(head)
  def to_activity_pub(nil), do: ActivityPub.Object
  def to_activity_pub("Object"), do: ActivityPub.Object
  def to_activity_pub("Person"), do: ActivityPub.Person
  def to_activity_pub("Create"), do: ActivityPub.Create
  def to_activity_pub("Note"), do: ActivityPub.Note

  defguard is_ap(o)
           when is_map(o) and :erlang.is_map_key(:__ap__, o) and
                  :erlang.map_get(:__struct__, :erlang.map_get(:__ap__, o)) ==
                    ActivityPub.Metadata

  defguard is_object(o) when is_ap(o) and :erlang.map_get(:object?, :erlang.map_get(:__ap__, o))
  defguard is_link(o) when is_ap(o) and not :erlang.map_get(:object?, :erlang.map_get(:__ap__, o))
  defguard is_actor(o) when is_ap(o) and :erlang.map_get(:actor?, :erlang.map_get(:__ap__, o))

  defguard is_activity(o)
           when is_ap(o) and :erlang.map_get(:activity?, :erlang.map_get(:__ap__, o))

  def persist(entity) when is_ap(entity) do
    ActivityPub.SQL.persist(entity)
  end

  def apply(%ActivityPub.Create{} = c) do
    ActivityPub.SQL.insert(c)
  end
end
