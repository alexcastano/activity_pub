defmodule ActivityPub.Person do
  use Ecto.Schema

  alias ActivityPub.{Person, Object, Metadata}
  alias Ecto.Changeset

  require Object.Heritage

  @primary_key false
  embedded_schema do
    field(:type, :string, default: "Person")

    Object.Heritage.is_object()

    field(:preferred_username, :string)
  end

  def static_fields(), do: __schema__(:fields)
  def string_static_fields(), do: static_fields() |> Enum.map(&to_string/1)

  def new(data \\ %{}) do
    %Person{}
    |> changeset(data)
    |> Changeset.apply_action(:insert)
  end

  def changeset(schema, data) do
    metadata = %Metadata{
      object?: true,
      actor?: true,
      activity?: false
    }

    schema
    |> Changeset.cast(data, [:preferred_username])
    |> Object.changeset(data)
    |> Changeset.put_embed(:__ap__, metadata)
  end
end
