defmodule ActivityPub.Object.Heritage do
  alias ActivityPub.{Metadata}

  defmacro is_object() do
    quote do
      embeds_one(:__ap__, Metadata)
      field(:__extensions, :map, default: %{})

      field(:id, :string)

      # field :attachment, {:array, ActivityPub.Entity}
      field(:attributed_to, ActivityPub.Entity, default: [])
      # field :audience, {:array, :map}

      # FIXME i18n
      field(:content, :string)

      # field :context, {:array, :map}

      # FIXME i18n
      field(:name, :string)

      field(:end_time, :utc_datetime)

      # embeds_many :generator, Object
      # embeds_many :icon, Object
    end
  end
end

defmodule ActivityPub.Object do
  use Ecto.Schema

  alias ActivityPub.{Object, Metadata}
  alias Ecto.Changeset

  require Object.Heritage

  @behaviour Access

  @primary_key false
  embedded_schema do
    field(:type, :string, default: "Object")

    Object.Heritage.is_object()
  end

  def static_fields(), do: __schema__(:fields)
  def string_static_fields(), do: static_fields() |> Enum.map(&to_string/1)

  # @fields [:attachment, :attributed_to, :audience, :content,
  #          :context, :name, :end_time, :generator, :icon,
  #          :inReplyTo, :location, :preview, :published, :replies,
  #          :startTime, :summary, :tag, :updated, :url, :to, :bto,
  #          :cc, :bcc, :mediaType, :duration]

  @fields [:id, :attributed_to, :content, :name, :end_time, :type]

  def new(data \\ %{}) do
    %Object{}
    |> changeset(data)
    |> Changeset.apply_action(:insert)
  end

  def changeset(schema, data) do
    metadata = %Metadata{
      object?: true
    }

    schema
    |> Changeset.cast(data, @fields)
    |> Changeset.validate_required([:type])
    |> insert_extensions(data)
    |> Changeset.put_embed(:__ap__, metadata)
  end

  defp insert_extensions(ch, data) do
    extensions = Map.drop(data, ch.data.__struct__.string_static_fields())
    Changeset.put_change(ch, :__extensions, extensions)
  end

  def fetch(%__MODULE__{} = o, key) when is_atom(key) do
    if key in static_fields(),
      do: Map.fetch(o, key),
      else: Map.fetch(o.__extensions, to_string(key))
  end

  def fetch(%__MODULE__{} = o, key) when is_binary(key) do
    if key in string_static_fields(),
      do: Map.fetch(o, String.to_atom(key)),
      else: Map.fetch(o.__extensions, key)
  end

  def fetch(_, _), do: :error

  # TODO implement get_and_update and pop
end
