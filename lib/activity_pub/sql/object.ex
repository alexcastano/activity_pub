defmodule ActivityPub.SQLObject do
  use Ecto.Schema
  require ActivityPub

  alias ActivityPub.{Object, SQLObject}

  @primary_key {:__local_id, :id, autogenerate: true}
  schema "activity_pub_objects" do
    field(:id, :string)
    field(:type, :string)
    field(:content, :string)
    field(:name, :string)
    field(:end_time, :utc_datetime)
    field(:__extensions, :map)
  end

  def insert(obj) when ActivityPub.is_object(obj) do
    changes = Map.take(obj, [:id, :type, :content, :name, :end_time, :__extensions])
    schema = struct!(SQLObject, changes)
    ActivityPub.Repo.insert(schema)
  end

  def load_query() do
    import Ecto.Query, only: [from: 2]

    from(object in SQLObject, as: :object)
  end

  def to_ap(%SQLObject{} = sql) do
    {:ok, obj} = sql |> get_props() |> Object.new()
    obj
  end

  def get_props(%SQLObject{} = sql) do
    Map.take(sql, [:id, :type, :content, :name, :end_time])
  end
end
