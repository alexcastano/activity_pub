defmodule ActivityPub.SQL.Entity do
  @behaviour Ecto.Type

  def type, do: :map

  def cast(id) when is_binary(id), do: ActivityPub.load(id)
  def cast(%{__ap__: _} = e), do: {:ok, e}
  def cast(map) when is_map(map), do: ActivityPub.parse(map)
  def cast(_), do: :error

  def load(map) when is_map(map), do: ActivityPub.parse(map)

  def dump(%{__ap__: _} = e), do: e
  def dump(_), do: :error
end
