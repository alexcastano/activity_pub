defmodule ActivityPub.SQLActor do
  use Ecto.Schema
  alias ActivityPub.SQLActor

  @primary_key {:__local_id, :id, autogenerate: false}
  schema "activity_pub_actors" do
    belongs_to(:object, ActivityPub.SQLObject,
      define_field: false,
      references: :__local_id,
      foreign_key: :__local_id
    )

    field(:preferred_username, :string)

    many_to_many(:attributed_objects, ActivityPub.SQLObject,
      join_through: "attributed_to_relation",
      join_keys: [actor_id: :__local_id, object_id: :__local_id]
    )
  end

  def insert(actor) do
    {:ok, obj} = ActivityPub.SQLObject.insert(actor)

    %SQLActor{
      object: obj,
      preferred_username: actor.preferred_username
    }
    |> ActivityPub.Repo.insert()
  end

  def load_query() do
    import Ecto.Query, only: [from: 2]

    from(actor in SQLActor,
      as: :actor,
      join: object in assoc(actor, :object),
      as: :object,
      preload: [object: object]
    )
  end

  def to_ap(%SQLActor{} = sql) do
    case get_props(sql) do
      %{type: "Person"} = props ->
        {:ok, obj} = ActivityPub.Person.new(props)
        obj
    end
  end

  def get_props(%SQLActor{} = sql) do
    ActivityPub.SQLObject.get_props(sql.object)
    |> Map.merge(%{preferred_username: sql.preferred_username})
  end
end
