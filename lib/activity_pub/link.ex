defmodule ActivityPub.Link do
  use Ecto.Schema

  alias Ecto.Changeset
  alias ActivityPub.{Link, Metadata}

  @fields [:href, :rel, :media_type, :name, :hreflang, :height, :width, :preview]

  embedded_schema do
    field(:href, :string)
    field(:rel, :string)
    field(:media_type, :string)
    field(:name, :string)
    field(:hreflang, :string)
    field(:height, :string)
    field(:width, :string)
    field(:preview, :string)

    embeds_one(:__ap__, Metadata)
  end

  def new(data \\ %{}) do
    metadata = %Metadata{}

    %Link{}
    |> changeset(data)
    |> Changeset.put_embed(:__ap__, metadata)
    |> Changeset.apply_action(:insert)
  end

  def changeset(schema, data) do
    schema
    |> Changeset.cast(data, @fields)
  end
end
