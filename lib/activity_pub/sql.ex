defmodule ActivityPub.SQL do
  require ActivityPub
  alias ActivityPub.{Repo, SQLActor, SQLObject}

  import Ecto.Query, only: [from: 2]

  def insert(actor) when ActivityPub.is_actor(actor) do
    SQLActor.insert(actor)
  end

  def insert(obj) when ActivityPub.is_object(obj) do
    SQLObject.insert(obj)
  end

  def load_actor(id) do
    load_by_id(id, SQLActor.load_query())
    |> SQLActor.to_ap()
  end

  def load_object(id) do
    load_by_id(id, SQLObject.load_query())
    |> SQLObject.to_ap()
  end

  def load_by_id(id, query) do
    from(
      [object: object] in query,
      where: object.id == ^id
    )
    |> Repo.one()
  end
end
