defmodule ActivityPub.Create do
  use Ecto.Schema

  alias ActivityPub.{Create, Person, Object, Metadata}
  alias Ecto.Changeset

  require Object.Heritage

  @primary_key false
  embedded_schema do
    field(:type, :string, default: "Create")

    Object.Heritage.is_object()

    field(:actor, ActivityPub.Entity, default: [])
    field(:object, ActivityPub.Entity, default: [])
    field(:target, ActivityPub.Entity, default: [])
    field(:origin, ActivityPub.Entity, default: [])
    field(:result, ActivityPub.Entity, default: [])
    field(:instrument, ActivityPub.Entity, default: [])
  end

  def new(data) do
    %Create{}
    |> changeset(data)
    |> Changeset.apply_action(:insert)
  end

  @fields [:actor, :object, :target, :origin, :result, :instrument]

  def changeset(schema, data) do
    metadata = %Metadata{
      object?: true,
      actor?: false,
      activity?: true
    }

    schema
    |> Changeset.cast(data, @fields)
    |> Object.changeset(data)
    |> Changeset.put_embed(:__ap__, metadata)
  end
end
