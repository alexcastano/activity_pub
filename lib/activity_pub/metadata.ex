defmodule ActivityPub.Metadata do
  # defstruct ancestors: []
  use Ecto.Schema

  embedded_schema do
    field(:object?, :boolean, default: false)
    field(:actor?, :boolean, default: false)
    field(:activity?, :boolean, default: false)
  end
end
