defmodule ActivityPub.Note do
  use Ecto.Schema

  alias ActivityPub.{Note, Object, Metadata}
  alias Ecto.Changeset

  require Object.Heritage

  @primary_key false
  embedded_schema do
    field(:type, :string, default: "Note")

    Object.Heritage.is_object()
  end

  def new(data \\ %{}) do
    %Note{}
    |> changeset(data)
    |> Changeset.apply_action(:insert)
  end
  defdelegate changeset(schema, data), to: ActivityPub.Object
end
